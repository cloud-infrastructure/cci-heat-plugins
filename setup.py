#!/usr/bin/env python

PROJECT = "cci-heat-plugins"

# Change docs/sphinx/conf.py too!
VERSION = '1.1.0'

from setuptools import setup, find_packages

setup(
    name=PROJECT,
    version=VERSION,
    description='CERN HEAT plugins',
    author='Cloud Infrastructure Team',
    author_email='cloud-infrastructure-developers@cern.ch',
    url='https://gitlab.cern.ch/cloud-infrastructure/cci-heat-plugins',
    classifiers=['License :: OSI Approved :: Apache Software License',
                 'Programming Language :: Python',
                 'Programming Language :: Python :: 2',
                 'Programming Language :: Python :: 2.7',
                 'Programming Language :: Python :: 3',
                 'Programming Language :: Python :: 3.2',
                 'Intended Audience :: Developers',
                 'Environment :: Console',
                 ],
    platforms=['Any'],
    scripts=[],
    provides=[],
    namespace_packages=[],
    packages=['heat'],
    include_package_data=True,
    data_files=[("/etc/cci",["etc/cci/cci-heat-plugins.conf", "etc/cci/heatfm.keytab"])],
)
