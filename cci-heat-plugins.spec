Name: cci-heat-plugins
Version: 1.2.0
Release: 1
Summary: CERN Cloud Infrastructure Heat Plugins
Source0: %{name}-%{version}.tar.gz
BuildArch: noarch

Group: CERN/Utilities
License: Apache License, Version 2.0
URL: https://openstack.cern.ch/

BuildRequires: python2-devel
BuildRequires: python-setuptools

Requires: python-suds
Requires: python-ldap
Requires: ai-tools

Requires(post): systemd
Requires(postun): systemd
BuildRequires: systemd

%description
Set of Heat plugins tailored for CERN Cloud Infrastructure

%prep
%setup -q -n %{name}-%{version}

%build
%{__python} setup.py build

%install
%{__python} setup.py install --skip-build --root %{buildroot} --install-lib %{_libdir}

%files
%defattr (-, root, root)
%doc LICENSE
%doc README.md
%{_libdir}/heat
%{_libdir}/*.egg-info
%config(noreplace) /etc/cci/cci-heat-plugins.conf
%config(noreplace) /etc/cci/heatfm.keytab

%post
%systemd_postun_with_restart openstack-heat-engine.service

%postun
%systemd_postun_with_restart openstack-heat-engine.service

%changelog
* Thu Aug 24 2017 Mathieu Velten <mathieu.velten@cern.ch> - 1.2.0
- Ignore certificate errors when contacting SSO

* Fri May 20 2016 Ricardo Rocha <ricardo.rocha@cern.ch> - 1.1.0
- Update for new release 1.1.0
- Move python code to sitelib
- Include README.md and LICENSE files in package

* Mon Feb 15 2016 Bruno Bompastor <bruno.bompastor@cern.ch> - 1.0.2
- Drop distribution from release

* Wed Feb 10 2016 Bruno Bompastor <bruno.bompastor@cern.ch> - 1.0.0
- First major verion

* Tue Feb 02 2016 Bruno Bompastor <bruno.bompastor@cern.ch> - 0.0.10
- Fix post restart

* Tue Feb 02 2016 Bruno Bompastor <bruno.bompastor@cern.ch> - 0.0.9
- Use systemd macros

* Fri Nov 27 2015 Bruno Bompastor <bruno.bompastor@cern.ch> - 0.0.8
- New version of roger plugin using roger API instead of ai-bs-vm

* Fri Oct 23 2015 Bruno Bompastor <bruno.bompastor@cern.ch> - 0.0.7
- Make sure config files are not replaced on upgrade

* Fri Oct 23 2015 Bruno Bompastor <bruno.bompastor@cern.ch> - 0.0.6
- Fix systemd cmds

* Fri Oct 23 2015 Bruno Bompastor <bruno.bompastor@cern.ch> - 0.0.5
- Restart heat on package upgrade

* Thu Oct 22 2015 Bruno Bompastor <bruno.bompastor@cern.ch> - 0.0.4
- Add Roger plugin

* Mon Sep 28 2015 Bruno Bompastor <bruno.bompastor@cern.ch> - 0.0.3
- Add default keytab file

* Fri Sep 25 2015 Bruno Bompastor <bruno.bompastor@cern.ch> - 0.0.2
- Add ai-tools requirement
- Add puppet plugin
- Change cci-heat-plugins.conf.sample to cci-heat-plugins.conf

* Fri Aug 21 2015 Bruno Bompastor <bruno.bompastor@cern.ch> - 0.0.1
- Initial version
