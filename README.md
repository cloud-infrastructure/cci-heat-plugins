CERN Cloud Infrastructure Heat Plugins
======================================

CERN Cloud Infrastructure Heat Plugins package provides integration
between OpenStack Heat project and CERN infrastucture using in-house APIs.

Release Management
------------------

### Changelog

In general, all the merge requests done in the repository should cover a new
feature, bug fix, etc... triggering a new version of the package.

This implies that every new feature should:

* Add a new entry in `cci-heat-plugins.spec`.
* Bump the version in `cci-heat-plugins.spec` and `setup.py`.

### Koji builds

The Koji builds will be triggered automatically after a merge request and the package
will be tagged as testing.

To tag it for the `qa` repository you can use:

    koji tag-pkg cci7-utils-qa cci-heat-plugins-0.0.8-1.el7

If you're happy with the qa version, you can release it in `stable`:

    koji tag-pkg cci7-utils-stable cci-heat-plugins-0.0.8-1.el7

