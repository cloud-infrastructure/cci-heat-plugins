from aitools.roger import RogerClient
import ConfigParser

CONF = "/etc/ai/ai.conf"

class rogerAPI:

    def __init__(self):
        conf_parser = ConfigParser.SafeConfigParser()
        conf_parser.read(CONF)
        host = conf_parser.get("roger", "roger_hostname")
        port = conf_parser.get("roger", "roger_port")
        timeout = conf_parser.get("roger", "roger_timeout")
        self.client = RogerClient(host=host, port=port, timeout=timeout, deref_alias=True)


    def update_or_create_state(self, hostname, appstate=False, message=None):
        return self.client.update_or_create_state(hostname, appstate, message)

    def get_state(self, hostname):
        return self.client.get_state(hostname)

    def delete_state(self, hostname):
        return self.client.delete_state(hostname)

