import os, sys, re
import ldap

class LdapCon(object):
    def __init__(self, ldap_server='ldap://xldap.cern.ch'):
        '''
        Constructor.
        
        @ivar: ldap_server: provide ldap server string, ie ldap://xldap.cern.ch 
        '''
        self.ldap_server = ldap_server
        self.conn = ldap.initialize(self.ldap_server)


class User(LdapCon):
    """
    wrap queries about users at CERN
    """
    def user_by_email(self, email,
                      base='OU=Users,OU=Organic Units,DC=cern,DC=ch'):
        filter = "(&(objectClass=user) (UserPrincipalName=%s))" % email
        try:
            res = self.conn.search_s(base, ldap.SCOPE_ONELEVEL, filter)
        except ldap.NO_SUCH_OBJECT, e:
            return False
        if not res:
            return False
        if len(res) > 1:
            # we shouldn't have more than one result for the same upn
            return False
        u = UserEntry()
        if not u.loads(res[0]):
            return False
        return u
        

class UserEntry(object):
    def __init__(self):
        self.cn = None
        self._fields = {'cn': lambda x: {'cn': x[0] },
                       'userPrincipalName': lambda x: {'email': x[0]},
                       'memberOf': lambda x: {'groups': list(map(lambda y: re.match("^CN=([^,]+),OU.+", y).groups()[0], x))},
                       }
        
    def loads(self, data):
        if not isinstance(data, tuple):
            return False
        if len(data) != 2:
            return False
        (self.long_cn, self.raw) = data
        for k,v in self.raw.items():
            if k in self._fields:
                for att, val in self._fields[k](v).items():
                    setattr(self, att, val)
        if self.cn:
            self.user = self.cn
            return True
        return False

class Egroup(LdapCon):
    '''
    wrap queries about egroups at CERN 
    '''
    def is_member_of(self, name, group, excl_disabled=True,
                     base_suffix='OU=Users,OU=Organic Units,DC=cern,DC=ch',
                     filter_suffix='OU=e-groups,OU=Workgroups,DC=cern,DC=ch)',
                     excl_disabled_prefix='(&(!(userAccountControl:1.2.840.113556.1.4.803:=2))(|'):
        """
        Is the given name a member of the group.
        
        @ivar name: name of group member to be searched for
        @ivar group: group name to be searched
        @ivar excl_disabled: exclude disabled accounts (recommended) 
        
        @ivar base_suffix: ldap base to which the name will be prepended
        @ivar filter_suffix: ldap filter to which the group will be prepended 
        """
        
        attrs = ['cn']
        base = "CN=%s,%s" % (name, base_suffix)
        filter = "(memberOf=CN=%s,%s" % (group, filter_suffix)
        nested_filter = "(memberOf:1.2.840.113556.1.4.1941:=CN=%s,%s" % (group, filter_suffix)
        if excl_disabled:
            filter = excl_disabled_prefix + filter + "))"
            nested_filter = excl_disabled_prefix +  nested_filter + "))"
        try:
            res = self.conn.search_s(base, ldap.SCOPE_SUBTREE, filter, attrs)
        except ldap.NO_SUCH_OBJECT, e:
            return False
        if not res:
            # try the (more expensive) search for nested groups
            res = self.conn.search_s(base, ldap.SCOPE_SUBTREE, nested_filter, attrs)
        if not res:
            return False
        else:
            if not isinstance(res, list):
                # not a result we expect
                return False
            for r in res:
                gm = GroupMember()
                if gm.loads(r):
                    if name == gm.cn:
                        # should only be one result anyway...
                        return True
        return False
    

class GroupMember(object):
    """
    parse & encapsulate ldap group membership
    """
    def __init__(self):
        self.cn = None

    def loads(self, entry):
        if not isinstance(entry, tuple):
            return False
        if len(entry) != 2:
            return False
        (cn_long, attrs) = entry
        self.cn_long = cn_long
        self.cn = attrs['cn'][0]
        return True
            
