from heat.common import exception
from heat.common.i18n import _
from heat.engine import attributes
from heat.engine import constraints
from heat.engine import properties
from heat.engine import resource
from heat.engine import support
from heat.engine import stack

import subprocess
import time
import os
from directory import Egroup
import simplejson as json

DEFAULT_FOREMAN_ENVIRONMENT = 'production'
DEFAULT_USERDATA_PATH = '/tmp/'
DEFAULT_OS_AUTH_URL = 'http://dummy.com'
DEFAULT_KEYTAB_FILE = '/etc/cci/heatfm.keytab'

class Puppet(resource.Resource):
    '''
    A resource which prepares a host for puppet configuration.
    '''

    PROPERTIES = (FQDN, ENVIRONMENT, HOSTGROUP) = ('fqdn', 'environment', 'hostgroup')

    ATTRIBUTES = (USER_DATA) = ('user_data')

    properties_schema = {
        FQDN: properties.Schema(
            properties.Schema.STRING,
            required=True,
            description=_('Full Qualified Domain Name.')
        ),
        ENVIRONMENT: properties.Schema(
            properties.Schema.STRING,
            required=False,
            description=_('Foreman Environment.')
        ),
        HOSTGROUP: properties.Schema(
            properties.Schema.STRING,
            required=True,
            description=_('Foreman Hostgroup.')
        ),
    }

    attributes_schema = {
        USER_DATA: attributes.Schema(
            description=_('User data.'),
            cache_mode=attributes.Schema.CACHE_NONE
        ),
    }

    def _init_creds(self):
        '''
        Initializes credentials.
        '''

        os.environ["OS_AUTH_URL"] = DEFAULT_OS_AUTH_URL
        result = subprocess.call(["klist", "-s"])
        if (result != 0 ):
            result = subprocess.call(["kinit", "-kt", DEFAULT_KEYTAB_FILE, "heatfm"])
            if (result != 0 ):
                raise Exception("Kerberos credentials not initialized.")

    def _generate_name(self, name):
        return name+'_'+str(int(time.time()))
    
    def handle_create(self):
        # Kerberos ticket
        self._init_creds()
        # FQDN
        fqdn = self.properties.get(self.FQDN)
        # Hostgroup
        foreman_hostgroup = self.properties.get(self.HOSTGROUP)
        # Enviroment
        if (self.properties.get(self.ENVIRONMENT)):
            foreman_environment = self.properties.get(self.ENVIRONMENT)
        else:
            foreman_environment = DEFAULT_FOREMAN_ENVIRONMENT

        # Check if the user is authorized on foreman
        user = self.context.user_id

        # Top-level hostgroup
        top_hostgroup = foreman_hostgroup.split("/")[0]

        try:
            result = subprocess.check_output(["ai-pwn", "show", "hostgroup", top_hostgroup])
        except subprocess.CalledProcessError:
            raise Exception("ai-pwn failed!!")

        groups = json.loads(result)['owners']
        Authorized = False
        for egroup in groups:
            if (Egroup().is_member_of(user, egroup)):
                Authorized = True
                break
        if (not Authorized):
            raise Exception("User '%s' is not authorized on hostgroup '%s', needs to be in one of '%s'" % (user, top_hostgroup, groups))
        
        # Userdata file
        userdata_file = self._generate_name(self.properties.get(self.FQDN))

        # Set env variable for project name
        os.environ["OS_PROJECT_NAME"] = self.context.tenant
        
        # Run ai-bs-vm and generate userdata
        try:
            result = subprocess.check_output(["ai-bs-vm", "--userdata-dump", DEFAULT_USERDATA_PATH + userdata_file, 
                                              "--nova-disable", "--foreman-hostgroup", foreman_hostgroup,
                                              "--foreman-environment", foreman_environment,
                                              "--nova-image", "dummy", "--nova-sshkey", "dummy_key", 
                                              "--nova-flavor", "m1.dummy", fqdn], stderr=subprocess.STDOUT)
        except subprocess.CalledProcessError as error:
            msg = error.output
            last = msg.split("\n")[-2]
            raise Exception(last)
        
        # Read userdata file
        try:
            with open (DEFAULT_USERDATA_PATH + userdata_file, "r") as myfile:
                data=myfile.read()
        except IOError as error:
            raise Exception(error)
        
        # Save userdata on DB and delete file
        self.data_set('puppet_userdata', data, redact=True)
        os.remove(DEFAULT_USERDATA_PATH + userdata_file)

    def handle_delete(self):
        self._init_creds()
        fqdn = self.properties.get(self.FQDN)
        
        try:
            result = subprocess.check_output(["ai-kill-vm", "--nova-disable", fqdn], stderr=subprocess.STDOUT)
        except:
            pass

    def _resolve_attribute(self, name):
        if name == self.USER_DATA:
            return self.data().get('puppet_userdata')


def resource_mapping():
    return {
        'CERN::CM::Puppet': Puppet,
    }

