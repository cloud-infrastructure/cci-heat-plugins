import time
import re

from WebSSO import *

from heat.common import exception
from heat.common.i18n import _
from heat.engine import attributes
from heat.engine import constraints
from heat.engine import properties
from heat.engine import resource
from heat.engine import support


class SSO(resource.Resource):
    '''
    A resource which registers a website on SSO.

    '''

    PROPERTIES = (HOST, AUTH) = ('host', 'auth')

    ATTRIBUTES = (APP_NAME, APP_URI, APP_AUTH) = ('app_name', 'app_uri', 'app_auth')

    properties_schema = {
        HOST: properties.Schema(
            properties.Schema.STRING,
            required=True,
            update_allowed=True,
            description=_('Application Host.')
        ),
        AUTH: properties.Schema(
            properties.Schema.LIST,
            update_allowed=True,
            constraints=[
                constraints.AllowedValues(['CERN Registered', 'CERN Shared', 
                                           'HEP Trusted', 'Verified External', 
                                           'Unverified External']),
            ],
            description=_("Application Authorization.")
        ),
    }

    attributes_schema = {
        APP_NAME: attributes.Schema(
            description=_('Application Name.'),
            cache_mode=attributes.Schema.CACHE_NONE
        ),
        APP_URI: attributes.Schema(
            description=_('Application URI.'),
            cache_mode=attributes.Schema.CACHE_NONE
        ),
        APP_AUTH: attributes.Schema(
            description=_('Application Basic Authorization.'),
            cache_mode=attributes.Schema.CACHE_NONE
        ),
    }

    client = WebSSO()

    def _is_valid_hostname(self, hostname):
        if len(hostname) > 255:
            return False
        if hostname[-1] == ".":
            hostname = hostname[:-1] # strip exactly one dot from the right, if present
        allowed = re.compile("(?!-)[A-Z\d-]{1,63}(?<!-)$", re.IGNORECASE)
        return all(allowed.match(x) for x in hostname.split("."))

    def _create_auth(self, auth_list):
        if auth_list:
            return ",".join(auth_list)
        else:
            return auth_list

    def _generate_url(self, host):
        hostname = host + '.cern.ch'
        if self._is_valid_hostname(hostname):
            return 'https://' + hostname + '/Shibboleth.sso/ADFS'
        else:
            raise Exception("The host is not valid.")

    def _generate_name(self):
        return 'heat-'+str(int(time.time()))

    def handle_create(self):
        name = self._generate_name() 
        uri = self._generate_url(self.properties.get(self.HOST))
        self.resource_id_set(uri)
        auth = self.properties.get(self.AUTH)

        self.client.createApplication(name, uri)
        if auth:
            auth = self._create_auth(auth)
            self.client.setBasicAuthorization(uri, auth)

    def check_create_complete(self, token):
        uri = self._generate_url(self.properties.get(self.HOST))
        result = self.client.getApplicationFromUri(uri)
        if not result:
            raise Exception("The App was not created.")
        return True

    def handle_update(self, json_snippet, templ_diff, prop_diff):

        if (self.HOST in prop_diff):
            # Delete
            uri = self.resource_id
            self.client.deleteApplicationFromUri(uri)
            # Create
            uri = self._generate_url(prop_diff.get(self.HOST))
            self.resource_id_set(uri)
            name = self._generate_name() 
            self.client.createApplication(name, uri)
            auth = self.properties.get(self.AUTH)
            if auth:
                auth = self._create_auth(auth)
                self.client.setBasicAuthorization(uri, auth)

        # Authorization
        if prop_diff.get(self.AUTH):
            uri = self.resource_id
            auth = self._create_auth(prop_diff.get(self.AUTH))
            self.client.setBasicAuthorization(uri, auth) 

        return prop_diff

    def check_update_complete(self, token):

        uri = self.resource_id
        if (self.HOST in token):
            result = self.client.getApplicationFromUri(uri)
            if result['uri'] != uri:
                raise Exception("The App was not created.")
            auth = self.client.getBasicAuthorization(uri)
            if auth == "Unauthorized.":
                raise Exception("App not authorized.")

        if token.get(self.AUTH):
            auth = self.client.getBasicAuthorization(uri)
            if auth == "Unauthorized.":
                raise Exception("App not authorized.")
            if auth != self._create_auth(token.get(self.AUTH)):
                raise Exception("Authorization was not updated.")

        return True

    def handle_delete(self):
        uri = self.resource_id
        try:
            self.client.deleteApplicationFromUri(uri)
        except:
            pass

    def check_delete_complete(self, token):
        uri = self.resource_id
        result = self.client.getApplicationFromUri(uri)
        if result:
            raise Exception("The App was not created.")
        return True

    def _resolve_attribute(self, name):
        uri = self.resource_id
        result = self.client.getApplicationFromUri(uri)
        if name == self.APP_NAME:
            return result['name']
        elif name == self.APP_URI:
            return result['uri']
        elif name == self.APP_AUTH:
            return self.client.getBasicAuthorization(uri)


def resource_mapping():
    return {
        'CERN::Web::SSO': SSO,
    }
