from suds.client import Client
from suds.transport.https import HttpAuthenticated
import ConfigParser
import ssl
import urllib2



CONF = "/etc/cci/cci-heat-plugins.conf"


class HttpInsecureAuthenticated(HttpAuthenticated):

    def u2handlers(self):
        ctx = ssl.create_default_context()
        ctx.check_hostname = False
        ctx.verify_mode = ssl.CERT_NONE

        handlers = HttpAuthenticated.u2handlers(self)
        handlers.append(urllib2.HTTPSHandler(context=ctx))
        return handlers


class WebSSO:

    def __init__(self):
        conf_parser = ConfigParser.SafeConfigParser()
        conf_parser.read(CONF)
        url = conf_parser.get("sso", "url")
        user = conf_parser.get("sso", "username")
        pwd = conf_parser.get("sso", "password")
        self.client = Client(url, transport=HttpInsecureAuthenticated(username=user, password=pwd))

    def createApplication(self, name, uri):
        return self.client.service.CreateApplication(name, uri)

    def deleteApplicationFromName(self, name):
        return self.client.service.DeleteApplicationFromName(name)

    def deleteApplicationFromUri(self, uri):
        return self.client.service.DeleteApplicationFromUri(uri)

    def getApplicationFromName(self, name):
        return self.client.service.GetApplicationFromName(name)
        
    def getApplicationFromUri(self, uri):
        return self.client.service.GetApplicationFromUri(uri)

    def getBasicAuthorization(self, uri):
        return self.client.service.GetBasicAuthorization(uri)

    def setBasicAuthorization(self, uri, auth):
        return self.client.service.SetBasicAuthorization(uri, auth)

