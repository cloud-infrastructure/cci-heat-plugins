from heat.common import exception
from heat.common.i18n import _
from heat.engine import attributes
from heat.engine import constraints
from heat.engine import properties
from heat.engine import resource
from heat.engine import support
from heat.engine import stack

import subprocess
import os

from rogerAPI import *

DEFAULT_OS_AUTH_URL = 'http://dummy.com'
DEFAULT_KEYTAB_FILE = '/etc/cci/heatfm.keytab'

class Roger(resource.Resource):
    '''
    A resource which manages roger registration.
    '''

    PROPERTIES = (FQDN, APPSTATE) = ('fqdn', 'appstate')

    properties_schema = {
        FQDN: properties.Schema(
            properties.Schema.STRING,
            required=True,
            description=_('Full Qualified Domain Name.')
        ),
        APPSTATE: properties.Schema(
            properties.Schema.STRING,
            required=False,
            description=_('Roger App State.')
        ),
    }

    client = rogerAPI()

    def _init_creds(self):
        '''
        Initializes credentials.
        '''

        os.environ["OS_AUTH_URL"] = DEFAULT_OS_AUTH_URL
        result = subprocess.call(["klist", "-s"])
        if (result != 0 ):
            result = subprocess.call(["kinit", "-kt", DEFAULT_KEYTAB_FILE, "heatfm"])
            if (result != 0 ):
                raise Exception("Kerberos credentials not initialized.")

    def handle_create(self):
        # Kerberos ticket
        self._init_creds()

        # FQDN
        fqdn = self.properties.get(self.FQDN)

        # Create on roger
        try:
            result = self.client.update_or_create_state(fqdn, appstate="build")
        except Exception, e:
            raise Exception(e)

        if "ERROR" in result:
            raise Exception(result)

    def check_create_complete(self, token):
        # Kerberos ticket
        self._init_creds()

        # FQDN
        fqdn = self.properties.get(self.FQDN)

        # Check with roger
        try:
            result = self.client.get_state(fqdn)
        except Exception, e:
            raise Exception(e)

        if "ERROR" in result:
            raise Exception(result)
        return True
        
    def handle_delete(self):
        # Kerberos ticket
        self._init_creds()

        # FQDN
        fqdn = self.properties.get(self.FQDN)

        # Create on roger
        try:
            result = self.client.delete_state(fqdn)
        except:
            pass


def resource_mapping():
    return {
        'CERN::CM::Roger': Roger,
    }

